import { Instance, Collection, Property, ObjectID, Transform } from 'iridium';
import { IProfile } from '../model/ProfileModel';
import { Alert } from '../model/AlertModel';
import * as BSON from 'bson';
import { mutateField } from '../util/DatabaseHelpers';

@Transform(document => document, (document, property, model) => {
	//this performs strict schema validation, deletes keys that dont exist on the schema
	// Object.keys(document).forEach(key => {
	// 	if(!model.schema.hasOwnProperty(key)) delete document[key];
	// });

	//this auto-generates an ObjectId for alert subdocs
	mutateField(document, "alerts", alerts => alerts.forEach(x => x.id = x.id || new BSON.ObjectID()));

	return document;
})
@Collection("profiles")
export class ProfileSchema extends Instance<IProfile, ProfileSchema> implements IProfile {

	@ObjectID
	public _id: string;

	@Property(String, true)
	public name: string;

	@Property([Alert], false)
	public alerts: Alert[];

	static onCreating(doc: ProfileSchema) {
		doc.alerts = doc.alerts || [];
	}

}
