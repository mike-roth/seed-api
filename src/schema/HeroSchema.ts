import { Instance, Collection, Property, ObjectID, Transform } from 'iridium';
import { IHero } from '../model/HeroModel';

@Transform(document => document, (document, property, model) => {
    Object.keys(document).forEach(key => {
        if(!model.schema.hasOwnProperty(key)) delete document[key];
    });
    return document;
})
@Collection("heroes")
export class HeroSchema extends Instance<IHero, HeroSchema> implements IHero {

    @ObjectID
    public _id: string;

    @Property(String, false)
    public power: string;

    @Property(Number, false)
    public amountPeopleSaved: number;

    @Property(String, true)
    public name: string;

}
