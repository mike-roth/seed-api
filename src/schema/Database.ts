import { Configuration, Core, Model, Transform } from 'iridium';
import { HeroSchema } from './HeroSchema';
import { decorate, injectable, unmanaged } from 'inversify';
import { IHero } from '../model/HeroModel';
import { IProfile } from '../model/ProfileModel';
import { ProfileSchema } from './ProfileSchema';

decorate(injectable(), Core);
decorate(unmanaged(), Core, 0);
decorate(unmanaged(), Core, 1);

@injectable()
export class Database extends Core {
    Hero: Model<IHero, HeroSchema> = new Model<IHero, HeroSchema>(this, HeroSchema);
    Profile: Model<IProfile, ProfileSchema> = new Model<IProfile, ProfileSchema>(this, ProfileSchema);

    constructor(uri: string, config?: Configuration) {
        // add your DB url to env var or hard code here
        super(process.env.MONGOHQ_URL, null);
    }

    onConnected() {
        return Promise.all([
            this.Hero.ensureIndexes(),
            this.Profile.ensureIndexes()
        ]).then(() => {
            console.log('onConnected()');
        });
    }

}