/**
 * Helper function for mutating a field of a document in a typesafe manner
 * while returning the document.
 * @param doc The document on which the field exists
 * @param field The field to mutate
 * @param mutate A method which is called to mutate the field
 */
export function mutateField<S, T extends keyof S>(doc: S, field: T, mutate: (field: S[T])=>void): S {
	mutate(doc[field]);
	return doc
}