import { IAlert } from '../model/AlertModel';

export interface IAlertService {
	create(profile_id: string, item: IAlert): Promise<IAlert>;
	retrieve(profile_id: string): Promise<IAlert[]>
}
