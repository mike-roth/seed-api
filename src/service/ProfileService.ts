import { inject, injectable } from 'inversify';
import TYPES from "../config/Types";
import { BaseService } from './base/BaseService';
import { IProfileDao } from '../dao/IProfileDao';
import { IProfileService } from './IProfileService';
import { ProfileSchema } from '../schema/ProfileSchema';
import { IProfile } from '../model/ProfileModel';

@injectable()
export class ProfileService extends BaseService<IProfileDao, IProfile, ProfileSchema> implements IProfileService {

	constructor (@inject(TYPES.IProfileDao) dao: IProfileDao) {
		super(dao);
	}

}
