export interface IBaseService<M,S> {
    retrieve(): Promise<S[]>;
    findById(_id: string): Promise<S>;
    create(item: M): Promise<S>;
    update(_id: string, item: M): Promise<any>;
    delete(_id: string): Promise<any>;
}
