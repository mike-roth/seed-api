import {injectable} from "inversify";
import { IBaseService } from './IBaseService';
import { IBaseDao } from '../../dao/base/IBaseDao';

@injectable()
export abstract class BaseService<T extends IBaseDao<M,S>, M, S> implements IBaseService<M,S> {

    protected _dao: T;

    constructor (dao: T) {
        this._dao = dao;
    }

    public create(item: M): Promise<S> {
        return new Promise<S>((resolve, reject) => {
            this._dao.create(item)
                .then((results) => {
                    resolve(results);
                })
                .catch((error) => {
                    reject(error);
                })
        });
    }

    public retrieve(): Promise<S[]> {
        return new Promise<S[]>((resolve, reject) => {
            this._dao.retrieve()
                .then((results) => {
                    resolve(results);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    public findById(_id: string): Promise<S> {
        return new Promise<S>((resolve, reject) => {
            this._dao.findById(_id)
                .then((results) => {
                    resolve(results || <S>{});
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    public update(_id: string, item: M): Promise<any> {
        return new Promise<number>((resolve, reject) => {
            this._dao.findById(_id)
                .then((results) => {
                    this._dao.update(_id, item)
                        .then((results) => {
                            resolve(results);
                        })
                        .catch((error) => {
                            reject(error);
                        })
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    public delete(_id: string): Promise<any> {
        return new Promise<number>((resolve, reject) => {
            this._dao.delete(_id)
                .then((results) => {
                    resolve(results || null);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

}
