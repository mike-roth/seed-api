import {ContainerModule, interfaces} from "inversify";
import Bind = interfaces.Bind;
import TYPES from "../config/Types";
import { IHeroService } from './IHeroService';
import { HeroService } from './HeroService';
import { AlertService } from './AlertService';
import { IAlertService } from './IAlertService';
import { ProfileService } from './ProfileService';
import { IProfileService } from './IProfileService';

class ServiceModule {

    static get config () {
        return new ContainerModule((bind: Bind) => {
            bind<IHeroService>(TYPES.IHeroService).to(HeroService).inSingletonScope();
            bind<IProfileService>(TYPES.IProfileService).to(ProfileService).inSingletonScope();
            bind<IAlertService>(TYPES.IAlertService).to(AlertService).inSingletonScope();
        });
    }

}

export = ServiceModule;