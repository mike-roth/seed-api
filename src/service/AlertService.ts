import { inject, injectable } from 'inversify';
import TYPES from "../config/Types";
import { IAlertService } from './IAlertService';
import { IAlert } from '../model/AlertModel';
import { IProfileDao } from '../dao/IProfileDao';

@injectable()
export class AlertService implements IAlertService {

	private _dao: IProfileDao;

	constructor (@inject(TYPES.IProfileDao) dao: IProfileDao) {
		this._dao = dao;
	}

	public retrieve(profile_id: string): Promise<IAlert[]> {
		return new Promise<IAlert[]>((resolve, reject) => {
			this._dao.findById(profile_id)
				.then((results) => {
					resolve(results.alerts);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	// public findById(_id: string): Promise<IAlert> {
	// 	return undefined;
	// }

	public create(profile_id: string, item: IAlert): Promise<IAlert> {
		return new Promise<IAlert>((resolve, reject) => {
			this._dao.findById(profile_id)
				.then((profile) => {
					profile.alerts.push(item);
					profile.save()
						.then((results) => {
							resolve(results.alerts.pop());
						})
						.catch((error) => {
							reject(error);
						});
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	// public update(_id: string, item: IAlert): Promise<number> {
	// 	return undefined;
	// }

	// public delete(_id: string): Promise<number> {
	// 	return undefined;
	// }

}
