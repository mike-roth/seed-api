import { inject, injectable } from 'inversify';
import TYPES from "../config/Types";
import { BaseService } from './base/BaseService';
import { IHeroDao } from '../dao/IHeroDao';
import { IHeroService } from './IHeroService';
import { HeroSchema } from '../schema/HeroSchema';
import { IHero } from '../model/HeroModel';

@injectable()
export class HeroService extends BaseService<IHeroDao, IHero, HeroSchema> implements IHeroService {

    constructor (@inject(TYPES.IHeroDao) dao: IHeroDao) {
        super(dao);
    }

}
