import { IBaseService } from './base/IBaseService';
import { ProfileSchema } from '../schema/ProfileSchema';
import { IProfile } from '../model/ProfileModel';

export interface IProfileService extends IBaseService<IProfile, ProfileSchema> {

}
