import { IBaseService } from './base/IBaseService';
import { HeroSchema } from '../schema/HeroSchema';
import { IHero } from '../model/HeroModel';

export interface IHeroService extends IBaseService<IHero, HeroSchema> {

}
