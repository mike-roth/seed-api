import { inject, injectable } from 'inversify';
import TYPES from '../config/Types';
import { Database } from '../schema/Database';
import { BaseDao } from './base/BaseDao';
import { IProfileDao } from './IProfileDao';
import { IProfile } from '../model/ProfileModel';
import { ProfileSchema } from '../schema/ProfileSchema';

@injectable()
export class ProfileDao extends BaseDao<IProfile, ProfileSchema> implements IProfileDao {
	constructor (@inject(TYPES.Database) db: Database) {
		super(db.Profile);
	}
}

Object.seal(ProfileDao);