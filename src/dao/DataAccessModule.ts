import {ContainerModule, interfaces} from "inversify";
import Bind = interfaces.Bind;
import TYPES from "../config/Types";
import { IHeroDao } from './IHeroDao';
import { HeroDao } from './HeroDao';
import { ProfileDao } from './ProfileDao';
import { IProfileDao } from './IProfileDao';

class DataAccessModule {

    static get config () {
        return new ContainerModule((bind: Bind) => {
            bind<IHeroDao>(TYPES.IHeroDao).to(HeroDao).inSingletonScope();
            bind<IProfileDao>(TYPES.IProfileDao).to(ProfileDao).inSingletonScope();
        });
    }

}

export = DataAccessModule;