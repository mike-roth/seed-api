import { IBaseDao } from './base/IBaseDao';
import { ProfileSchema } from '../schema/ProfileSchema';
import { IProfile } from '../model/ProfileModel';

export interface IProfileDao extends IBaseDao<IProfile, ProfileSchema> {
}
