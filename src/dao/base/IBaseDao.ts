import Bluebird = require('bluebird');

export interface IBaseDao<M,S> {
    retrieve(): Bluebird<S[]>;
    findById(_id: string): Bluebird<S>;
    create(item: M): Bluebird<S>;
    update(_id: string, item: M): Promise<any>;
    delete(_id: string): Bluebird<number>;
}
