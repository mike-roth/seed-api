import { injectable, unmanaged } from "inversify";
import { Model, toObjectID } from 'iridium';
import { IBaseDao } from './IBaseDao';
import Bluebird = require('bluebird');

@injectable()
export abstract class BaseDao<M,S> implements IBaseDao<M,S> {

    protected _collection: Model<M,S>;

    constructor (@unmanaged() collection: Model<M,S>) {
        this._collection = collection;
    }

    create(item: M): Bluebird<S> {
        return this._collection.create(item);
    }

    retrieve(): Bluebird<S[]> {
        return this._collection.find().toArray();
    }

    update(_id: string, item: M): Promise<any> {
        return this._collection.collection.update({_id: toObjectID(_id)}, { $set: item });
    }

    delete(_id: string): Bluebird<number> {
        return this._collection.remove({_id: _id});
    }

    findById(_id: string): Bluebird<S> {
        return this._collection.findOne(_id);
    }

}
