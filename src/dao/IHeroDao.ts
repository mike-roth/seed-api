import { IBaseDao } from './base/IBaseDao';
import { HeroSchema } from '../schema/HeroSchema';
import { IHero } from '../model/HeroModel';

export interface IHeroDao extends IBaseDao<IHero, HeroSchema> {

}
