import { inject, injectable } from 'inversify';
import TYPES from '../config/Types';
import { Database } from '../schema/Database';
import { BaseDao } from './base/BaseDao';
import { IHeroDao } from './IHeroDao';
import { HeroSchema } from '../schema/HeroSchema';
import { IHero } from '../model/HeroModel';

@injectable()
export class HeroDao extends BaseDao<IHero, HeroSchema> implements IHeroDao {
    constructor (@inject(TYPES.Database) db: Database) {
        super(db.Hero);
    }
}

Object.seal(HeroDao);