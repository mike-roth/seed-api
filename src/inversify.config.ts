import {Container} from 'inversify';
import TYPES from './config/Types';
import ControllerModule = require('./controller/ControllerModule');
import ServiceModule = require('./service/ServiceModule');
import DataAccessModule = require('./dao/DataAccessModule');
import { Database } from './schema/Database';

const container = new Container();
container.bind<Database>(TYPES.Database).to(Database).inSingletonScope();
container.load(DataAccessModule.config);
container.load(ServiceModule.config);
container.load(ControllerModule.config);

export default container;