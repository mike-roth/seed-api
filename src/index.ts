import 'reflect-metadata';
import { Database } from './schema/Database';
import { InversifyExpressServer } from "inversify-express-utils";
import bodyParser = require("body-parser");
import TYPES from './config/Types';
import container from './inversify.config';

const database: Database = container.get<Database>(TYPES.Database);

let server = new InversifyExpressServer(container);
let port = parseInt(process.env.PORT, 10) || 3000;

server.setConfig((app) => {
    app.set("port", port);
    app.use(bodyParser.json());
});

let app = server.build();

app.listen(port, () => {
    console.log("Node app is running at port:" + port);
    database.connect();
});
