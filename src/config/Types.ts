const TYPES = {
    Database: Symbol('Database'),
    IHeroService: Symbol('IHeroService'),
    IHeroDao: Symbol('IHeroDao'),
    IAlertService: Symbol('IAlertService'),
    IProfileService: Symbol('IProfileService'),
    IProfileDao: Symbol('IProfileDao'),
};

export default TYPES;