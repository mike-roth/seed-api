const TAGS = {
    HeroController: 'HeroController',
    ProfileController: 'ProfileController',
    AlertController: 'AlertController'
};

export default TAGS;
