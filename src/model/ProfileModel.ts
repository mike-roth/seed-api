import { Alert } from './AlertModel';

export interface IProfile {
	_id?: string;
	name: string;
	alerts?: Alert[];
}

export class Profile implements IProfile {
	name: string;
	alerts?: Alert[];

	constructor() {

	}
}