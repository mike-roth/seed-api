export interface IAlert {
	_id?: string;
	status: string;
	case_id: string;
	alert_type: string;
	check_in_duration: string;
}

export class Alert implements IAlert {
	status: string;
	case_id: string;
	alert_type: string;
	check_in_duration: string;

	constructor() {

	}
}