export interface IHero {
    _id?: string;
    power: string;
    amountPeopleSaved: number;
    name: string;
}

export class Hero implements IHero {
    power: string;
    amountPeopleSaved: number;
    name: string;

    constructor() {

    }
}