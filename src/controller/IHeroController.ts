
import { ICreateController, IDeleteController, IReadController, IUpdateController } from './base/IBaseController';
import { IHero } from '../model/HeroModel';

export interface IHeroController extends ICreateController<IHero>, IReadController<IHero>, IUpdateController, IDeleteController {
    // getAll(req: express.Request, res: express.Response): IHeroModel[];
}
