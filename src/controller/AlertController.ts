import e = require("express");
import {Controller, Post, Get, Put, Delete} from "inversify-express-utils";
import {injectable, inject} from "inversify";
import TYPES from "../config/Types";
import { IAlertService } from '../service/IAlertService';
import { IAlertController } from './IAlertController';
import { IAlert } from '../model/AlertModel';

@injectable()
@Controller('/profiles/:id/alerts')
export class AlertController implements IAlertController {

	private _service: IAlertService;

	constructor(@inject(TYPES.IAlertService) service: IAlertService) {
		this._service = service;
	}

	@Post('/')
	public create(req: e.Request, res: e.Response): Promise<IAlert> {
		return this._service.create(req.params.id, req.body);
	}

	@Get('/')
	public retrieve(req: e.Request, res: e.Response): Promise<IAlert[]> {
		return this._service.retrieve(req.params.id);
	}

	@Get('/:id')
	public findById(req: e.Request, res: e.Response): Promise<IAlert> {
		// return this._service.findById(req.params.id);
		return null;
	}

	@Put('/:id')
	public update(req: e.Request, res: e.Response): Promise<any> {
		// return this._service.update(req.params.id, req.body);
		return null;
	}

	@Delete('/:id')
	public delete(req: e.Request, res: e.Response): Promise<any> {
		// return this._service.delete(req.params.id);
		return null;
	}

}
