import express = require("express");

export interface ICreateController<M> {
	create(req: express.Request, res: express.Response): Promise<M>;
}

export interface IDeleteController {
	delete(req: express.Request, res: express.Response): Promise<any>;
}

export interface IReadController<M> {
	retrieve(req: express.Request, res: express.Response): Promise<M[]>;
	findById(req: express.Request, res: express.Response): Promise<M>
}

export interface IUpdateController {
	update(req: express.Request, res: express.Response): Promise<any>;
}
