import e = require("express");
import {Controller, Post, Get, Put, Delete} from "inversify-express-utils";
import {injectable, inject} from "inversify";
import TYPES from "../config/Types";
import { IHeroService } from '../service/IHeroService';
import { IHeroController } from './IHeroController';
import { IHero } from '../model/HeroModel';

@injectable()
@Controller('/heroes')
export class HeroController implements IHeroController {

    private _service: IHeroService;

    constructor(@inject(TYPES.IHeroService) service: IHeroService) {
        this._service = service;
    }

    @Post('/')
    public create(req: e.Request, res: e.Response): Promise<IHero> {
        return this._service.create(req.body);
    }

    @Get('/')
    public retrieve(req: e.Request, res: e.Response): Promise<IHero[]> {
        return this._service.retrieve();
    }

    @Get('/:id')
    public findById(req: e.Request, res: e.Response): Promise<IHero> {
        return this._service.findById(req.params.id);
    }

    @Put('/:id')
    public update(req: e.Request, res: e.Response): Promise<any> {
        return this._service.update(req.params.id, req.body);
    }

    @Delete('/:id')
    public delete(req: e.Request, res: e.Response): Promise<any> {
        return this._service.delete(req.params.id);
    }

}
