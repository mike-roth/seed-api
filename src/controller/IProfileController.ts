import { ICreateController, IDeleteController, IReadController, IUpdateController } from './base/IBaseController';
import { IProfile } from '../model/ProfileModel';

export interface IProfileController extends ICreateController<IProfile>, IReadController<IProfile>, IUpdateController, IDeleteController {

}
