import e = require("express");
import {Controller, Post, Get, Put, Delete} from "inversify-express-utils";
import {injectable, inject} from "inversify";
import TYPES from "../config/Types";
import { IProfileService } from '../service/IProfileService';
import { IProfileController } from './IProfileController';
import { IProfile } from '../model/ProfileModel';

@injectable()
@Controller('/profiles')
export class ProfileController implements IProfileController {

	private _service: IProfileService;

	constructor(@inject(TYPES.IProfileService) service: IProfileService) {
		this._service = service;
	}

	@Post('/')
	public create(req: e.Request, res: e.Response): Promise<IProfile> {
		return this._service.create(req.body);
	}

	@Get('/')
	public retrieve(req: e.Request, res: e.Response): Promise<IProfile[]> {
		return this._service.retrieve();
	}

	@Get('/:id')
	public findById(req: e.Request, res: e.Response): Promise<IProfile> {
		return this._service.findById(req.params.id);
	}

	@Put('/:id')
	public update(req: e.Request, res: e.Response): Promise<any> {
		return this._service.update(req.params.id, req.body);
	}

	@Delete('/:id')
	public delete(req: e.Request, res: e.Response): Promise<any> {
		return this._service.delete(req.params.id);
	}

}
