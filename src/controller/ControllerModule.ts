import {ContainerModule, interfaces} from "inversify";
import Bind = interfaces.Bind;
import {TYPE} from "inversify-express-utils";
import TAGS from "../config/Tags";
import { IHeroController } from './IHeroController';
import { HeroController } from './HeroController';
import { IAlertController } from './IAlertController';
import { AlertController } from './AlertController';
import { IProfileController } from './IProfileController';
import { ProfileController } from './ProfileController';

class ControllerModule {

    static get config () {
        return new ContainerModule((bind: Bind) => {
            bind<IHeroController>(TYPE.Controller).to(HeroController).inSingletonScope().whenTargetNamed(TAGS.HeroController);
            bind<IProfileController>(TYPE.Controller).to(ProfileController).inSingletonScope().whenTargetNamed(TAGS.ProfileController);
            bind<IAlertController>(TYPE.Controller).to(AlertController).inSingletonScope().whenTargetNamed(TAGS.AlertController);
        });
    }

}

export = ControllerModule;