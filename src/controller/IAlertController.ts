import { ICreateController, IDeleteController, IReadController, IUpdateController } from './base/IBaseController';
import { IAlert } from '../model/AlertModel';

export interface IAlertController extends ICreateController<IAlert>, IReadController<IAlert>, IUpdateController, IDeleteController {

}
